# postoplan_test

Для развёртывания:

    docker-compose up -d
    docker-compose exec php-cli composer install
    docker-compose exec php-cli php bin/console doctrine:migrations:migrate 
    docker-compose exec php-cli php bin/console doctrine:fixtures:load 
    docker-compose exec php-cli chown -R www-data:www-data ./data/counters 
    docker-compose exec php-cli php bin/console BannerCounterTake 

