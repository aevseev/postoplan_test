<?php

namespace App\Command;

use App\Service\BannerService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BannerCounterTakeCommand extends Command
{
    protected static $defaultName = 'BannerCounterTake';
    protected static $defaultDescription = 'Зафиксировать счётчки баннера';

    protected ContainerInterface $container;
    protected BannerService $bannerService;

    public function __construct(ContainerInterface $container, BannerService $bannerService)
    {
        parent::__construct();
        $this->container = $container;
        $this->bannerService = $bannerService;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));
        }

        if ($input->getOption('option1')) {
            // ...
        }

        while (true) {
            $this->bannerService->takeCounter('banner');
            sleep(26);
        }

        return Command::SUCCESS;
    }
}
