<?php

namespace App\Controller;

use App\Service\BannerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api/banner/show", name="api_banner_show")
     */
    public function apiBannerShow(BannerService $bannerService): Response
    {
        $bannerService->addCounter('banner');
        return new JsonResponse(['ok' => true]);
    }
}
