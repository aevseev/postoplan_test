<?php

namespace App\Controller;

use App\Entity\BannerCounter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lk", name="app_lk")
 */
class LKController extends AbstractController
{
    /**
     * @Route("", name="")
     */
    public function index(EntityManagerInterface $em): Response
    {

        $bannerCounterQB = $em->getRepository(BannerCounter::class)
            ->createQueryBuilder('bc')
            ->orderBy('bc.dateAt', 'DESC')
            ->setMaxResults(1);

        $bannerCounter = $bannerCounterQB->getQuery()->getOneOrNullResult();

        return $this->render('lk/index.html.twig', [
            'controller_name' => 'LKController',
            'bannerCounter' => $bannerCounter
        ]);
    }
}
