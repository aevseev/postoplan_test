<?php

namespace App\Entity;

use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 */
class Author
{

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Author2Book::class, mappedBy="author")
     */
    private $author2Books;

    public function __construct()
    {
        $this->author2Books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Author2Book[]
     */
    public function getAuthor2Books(): Collection
    {
        return $this->author2Books;
    }

    public function addAuthor2Book(Author2Book $author2Book): self
    {
        if (!$this->author2Books->contains($author2Book)) {
            $this->author2Books[] = $author2Book;
            $author2Book->setAuthor($this);
        }

        return $this;
    }

    public function removeAuthor2Book(Author2Book $author2Book): self
    {
        if ($this->author2Books->removeElement($author2Book)) {
            // set the owning side to null (unless already changed)
            if ($author2Book->getAuthor() === $this) {
                $author2Book->setAuthor(null);
            }
        }

        return $this;
    }
}
