<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity=Author2Book::class, mappedBy="book", cascade={"persist"}, orphanRemoval=true)
     */
    private $author2Books;

    public function __construct()
    {
        $this->author2Books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection|Author2Book[]
     */
    public function getAuthor2Books(): Collection
    {
        return $this->author2Books;
    }

    public function addAuthor2Book(Author2Book $author2Book): self
    {
        if (!$this->author2Books->contains($author2Book)) {
            $this->author2Books[] = $author2Book;
            $author2Book->setBook($this);
        }

        return $this;
    }

    public function removeAuthor2Book(Author2Book $author2Book): self
    {
        if ($this->author2Books->removeElement($author2Book)) {
            // set the owning side to null (unless already changed)
            if ($author2Book->getBook() === $this) {
                $author2Book->setBook(null);
            }
        }

        return $this;
    }
}
