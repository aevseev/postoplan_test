<?php

namespace App\Form;

use App\Entity\Author;
use App\Entity\Author2Book;
use App\Entity\Book;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Event\PostSubmitEvent;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    private ?EntityManagerInterface $em = null;

    public function __construct(EntityManagerInterface $em) {
        $this->em = $em;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Book $data */
        $data = $builder->getData();
        $builder
            ->add('title')
            ->add('authors', EntityType::class, [
                'class' => Author::class,
                'mapped' => false,
                'multiple' => true,
                'data' => $data ? array_map(function ($item) {return $item->getAuthor();}, $data->getAuthor2Books()->getValues()) : []
            ])
        ;

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (PostSubmitEvent $postSubmitEvent) {
            /** @var Book $book */
            $book = $postSubmitEvent->getData();
            $form = $postSubmitEvent->getForm();

            foreach ($book->getAuthor2Books() as $a2b) {
                $found = false;
                foreach ($form->get('authors')->getData() as $author) {
                    if ($author === $a2b->getAuthor()) {
                        $found = true;
                        break;
                    }
                }
                if (!$found) {
                    $book->removeAuthor2Book($a2b);
                }
            }

            foreach ($form->get('authors')->getData() as $author) {
                $author2Book = new Author2Book();
                $author2Book->setAuthor($author);
                $book->addAuthor2Book($author2Book);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Book::class,
        ]);
    }
}
