<?php

namespace App\Repository;

use App\Entity\Author2Book;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Author2Book|null find($id, $lockMode = null, $lockVersion = null)
 * @method Author2Book|null findOneBy(array $criteria, array $orderBy = null)
 * @method Author2Book[]    findAll()
 * @method Author2Book[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Author2BookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Author2Book::class);
    }

    // /**
    //  * @return Author2Book[] Returns an array of Author2Book objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Author2Book
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
