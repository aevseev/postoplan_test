<?php

namespace App\Repository;

use App\Entity\BannerCounter;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BannerCounter|null find($id, $lockMode = null, $lockVersion = null)
 * @method BannerCounter|null findOneBy(array $criteria, array $orderBy = null)
 * @method BannerCounter[]    findAll()
 * @method BannerCounter[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BannerCounterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BannerCounter::class);
    }

    // /**
    //  * @return BannerCounter[] Returns an array of BannerCounter objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BannerCounter
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
