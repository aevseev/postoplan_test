<?php

namespace App\Service;

use App\Entity\BannerCounter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BannerService
{
    private ContainerInterface $container;
    private EntityManagerInterface $em;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->container = $container;
        $this->em = $em;
    }

    function addCounter($bannerName)
    {
        $counterPath = $this->container->getParameter('counter_path');

        $file = fopen($counterPath . "/$bannerName.txt", 'a');
        flock($file, LOCK_EX);
        $counter = file_get_contents($counterPath . "/$bannerName.txt");
        ftruncate($file, 0);
        fwrite($file, (int)$counter + 1);
        flock($file, LOCK_UN);
        fclose($file);
    }

    public function takeCounter(string $bannerName): void
    {
        $counterPath = $this->container->getParameter('counter_path');
        $counter = file_get_contents($counterPath . "/$bannerName.txt");

        $bannerCounter = new BannerCounter();
        $bannerCounter->setAmount((int)$counter);
        $bannerCounter->setDateAt(new \DateTime('now'));
        $this->em->persist($bannerCounter);
        $this->em->flush();
    }
}
